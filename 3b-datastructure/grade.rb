def studentGrade(evaluation)
  total = 0
  evaluation.each do |key, value|
    total = total + value
  end
  average = total / evaluation.size
  above = 0
  evaluation.each do |key, value|
    if value > average
      above = above + 1
    end
  end  
  puts "Class average grade is #{average} and number of students with above-average grade are #{above}"
end

def convertGrade(evaluation)
  puts "Student | Grade | Alphabet-based grade"
  evaluation.each do |key, value|
    if value >= 80
      puts "#{key} | #{value} | A"
    elsif value < 80 && value >=70
      puts "#{key} | #{value} | B"
    elsif value < 70 && value >=60
      puts "#{key} | #{value} | C"
    elsif value < 60 && value >=40
      puts "#{key} | #{value} | D"
    elsif value < 40
      puts "#{key} | #{value} | E"
    else
      puts "#{key} | #{value} | Not defined"
    end
  end
end

evaluation = {
  "Alice": 75,
  "Bob": 80,
  "Candra": 100,
  "Dede": 64,
  "Eka": 90
}

studentGrade(evaluation)
convertGrade(evaluation)