names = [
	'Ahmad', 
	'Tono', 
	'Tini', 
	'Bambang', 
	'Agus', 
	'Agung'
]

names.each {
	|name|
	if (name[0,1] == "A") && (name.size % 2 != 0)
		puts name.upcase
	elsif name[0,1] == "T"
		puts name.downcase
	else
		puts name.reverse.capitalize
	end
}