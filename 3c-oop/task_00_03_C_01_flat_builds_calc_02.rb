# frozen_string_literal: true

# Shape

class Shape
  attr_accessor :length
  def initialize(length)
    @length = length
  end

  def area
    @length * @length
  end

  def circumference
    6 * @length
  end
end

# Circle
class Circle < Shape
  attr_accessor :jari_jari
  def initialize(jari_jari)
    @r = jari_jari
  end

  def area
    3.14 * @r * @r
  end

  def circumference
    2 * 3.14 * @r
  end
end

# Rectangle
class Rectangle < Shape
  attr_accessor :height, :width
  def initialize(height, width)
    @height = height
    @width = width
  end

  def area
    @height * @width
  end

  def circumference
    2 * (@height + @width)
  end
end

# Calc
class Calc
  def self.go
    puts '1. Circle'
    puts '2. Rectangle'
    puts 'Please choose operation to calculate area and circumference 1-2: '
    opt = gets.chomp.to_i
    case opt
    when 1
      puts 'Enter r: '
      r = gets.chomp.to_i
      circle = Circle.new(r)
      puts "Circle area is #{circle.area.round(2)}"
      puts "its circumference is #{circle.circumference.round(2)}"
    when 2
      puts 'Enter height: '
      height = gets.chomp.to_i
      puts 'Enter width: '
      width = gets.chomp.to_i
      rect = Rectangle.new(height, width)
      puts "Rectangle area is #{rect.area.round(2)}"
      puts "its circumference is #{rect.circumference.round(2)}"
    end
  end
end

Calc.go
