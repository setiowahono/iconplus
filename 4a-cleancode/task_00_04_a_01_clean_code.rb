# frozen_string_literal: true

def add(numb1, numb2)
  v = numb1 + numb2
  puts "Hasil penjumlahan: #{v}"
end

def subt(numb1, numb2)
  v = numb1 - numb2
  puts "Hasil pengurangan: #{v}"
end

def mult(numb1, numb2)
  v = numb1 * numb2
  puts "Hasil perkalian: #{v}"
end

add(1, 2)
subt(5, 2)
mult(4, 5)
